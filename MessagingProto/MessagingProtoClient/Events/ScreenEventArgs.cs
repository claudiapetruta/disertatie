﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.Events
{
    public class ScreenEventArgs : EventArgs
    {
        public IShowScreen NewScreen { get; set; }
        public IShowScreen OldScreen { get; set; }
        public Boolean CloseOldScreen { get; set; }

        public ScreenEventArgs(IShowScreen oldScreen, IShowScreen newScreen, Boolean closeOldScreen)
        {
            OldScreen = oldScreen;
            NewScreen = newScreen;
            CloseOldScreen = CloseOldScreen;
        }
        public ScreenEventArgs()
        {
            OldScreen = null;
            NewScreen = null;
            CloseOldScreen = false;
        }
    }
}
