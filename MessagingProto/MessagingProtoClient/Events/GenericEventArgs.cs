﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.Events
{
    public class GenericEventArgs
    {
        public Object GenericArg { get; set; }
    }
}
