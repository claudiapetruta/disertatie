﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.Events
{
    public interface IShowScreen 
    {
         event EventHandler<ScreenEventArgs> ShowScreenEvent;
    }
}
