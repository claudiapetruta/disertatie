﻿using System;
using Caliburn.Micro;
using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using CommonClasses.CommonUtils;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Events;
using MessagingProtoClient.Models;

namespace MessagingProtoClient.ViewModels
{
    class ReplyPendingViewModel : Screen, IShowScreen
    {
        ClientConnectionHandler connection;
        FriendReplyModel friendData;

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public ReplyPendingViewModel (ClientConnectionHandler con, FriendReplyModel friendReply)
        {
            friendData = friendReply;
            connection = con;
        }

        public void AcceptFriendRequest()
        {
            RequestEncoder encoder = new RequestEncoder();
            friendData.Status = RelationshipStatus.Accepted;
            string infoToSend = encoder.EncodeRequest(RequestTypes.ChangeFriendshipStatus, friendData);
            connection.SendInformation(infoToSend);
            TryClose();
        }

        public void DenyFriendRequest()
        {
            RequestEncoder encoder = new RequestEncoder();
            string infoToSend = encoder.EncodeRequest(RequestTypes.DeleteFriendship, friendData);
            connection.SendInformation(infoToSend);
            TryClose();
        }

        public void BlockFriendRequest()
        {
            RequestEncoder encoder = new RequestEncoder();
            friendData.Status = RelationshipStatus.Blocked;
            string infoToSend = encoder.EncodeRequest(RequestTypes.ChangeFriendshipStatus, friendData);
            connection.SendInformation(infoToSend);
            TryClose();
        }
    }
}
