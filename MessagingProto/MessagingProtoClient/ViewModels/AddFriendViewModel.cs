﻿using Caliburn.Micro;
using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using CommonClasses.CommonUtils;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Events;
using MessagingProtoClient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.ViewModels
{
    [Export(typeof(RegistrationViewModel))]
    class AddFriendViewModel : Screen,IShowScreen
    {
        ClientConnectionHandler connection;

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public string InitiatorUsername { get; set; }

        public string TargetUsername { get; set; }

        [ImportingConstructor]
        public AddFriendViewModel(ClientConnectionHandler conn)
        {
            connection = conn;
            connection.requestDecoder.AddFriendsReply += AddFriendsReplyCallback;
        }

        public AddFriendViewModel() { }

        public void AddFriend()
        {
            RequestEncoder encoder = new RequestEncoder();
  
            InitiatorUsername = CurrentUser.Username;
            FriendsModel relationship = new FriendsModel(InitiatorUsername, TargetUsername, RelationshipStatus.Pending);

            string infoToSend = encoder.EncodeRequest(RequestTypes.AddFriend, relationship);
            connection.SendInformation(infoToSend);

        }

        public void AddFriendsReplyCallback(object sender, GenericEventArgs e)
        {
            string message = (string)e.GenericArg;
            ChangeScreenEventMethod(new NotificationPopUpViewModel(message));
            Console.WriteLine("Create notfication pop-up model");
            connection.requestDecoder.AddFriendsReply -= AddFriendsReplyCallback;
            TryClose();
        }

        protected virtual void ChangeScreenEventMethod(IShowScreen newScreen)
        {
            ScreenEventArgs e = new ScreenEventArgs(this, newScreen, true);

            EventHandler<ScreenEventArgs> handler = ShowScreenEvent;
            if (handler != null)
            {
                handler(this, e);
            }
            TryClose();
        }
    }
}
