﻿using Caliburn.Micro;
using CommonClasses.CommonModels;
using CommonClasses.CommonEnums;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Utils;
using MessagingProtoClient.Validation;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using CommonClasses.CommonUtils;
using System;
using MessagingProtoClient.Events;

namespace MessagingProtoClient.ViewModels
{
    [Export(typeof(RegistrationViewModel))]
    public class RegistrationViewModel : Screen, IShowScreen
    {
        ClientConnectionHandler connection;
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        private string ServerResponse { get; set; }
        private string error;

        private string fieldsError;

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public string FieldsError
        {
            get
            {
                return fieldsError;
            }
            set
            {
                fieldsError = value;
                NotifyOfPropertyChange(() => FieldsError);
            }
        }

        public string Error
        {
            get
            {
                return error;
            }
            set
            {
                error = value;
                NotifyOfPropertyChange(() => Error);
            }
        }

        public RegistrationViewModel(ClientConnectionHandler con)
        {
            connection = con;
        }

        public void RegisterAccount()
        {
            string hashedPassword = PasswordHashing.GeneratePasswordHash(Password);
            string hashedConfirmPassword = PasswordHashing.GeneratePasswordHash(ConfirmPassword);
            RequestEncoder encoder = new RequestEncoder();

            if(!PasswordValidation.ConfirmPassword(hashedPassword, hashedConfirmPassword))
            {
                FieldsError = "The passwords do not match!";
            }
            else if (EmailValidation.ValidateEmailFormat(Email) == false)
            {
                FieldsError = "The email format is invalid!";
            }
            else
            {
                AccountModel account = new AccountModel(Username, hashedPassword, Email);
                string infoToSend = encoder.EncodeRequest(RequestTypes.Registration, account);
                ServerResponse = connection.SendReceiveInformation(infoToSend);
            }

            if(ServerResponse == "True")
            {
                ChangeScreenEventMethod(new LoginViewModel(connection));
            }
            else
            {
                FieldsError = "The username or email address is not unique.";
                Error = "The account couldn't be created.";
            }
            
        }

        public void SetPassword(PasswordBox box)
        {
            Password = box.Password;
        }

        public void SetConfirmPassword(PasswordBox box)
        {
            ConfirmPassword = box.Password;
        }

        protected virtual void ChangeScreenEventMethod(IShowScreen newScreen)
        {
            ScreenEventArgs e = new ScreenEventArgs(this,newScreen,true);
            
            EventHandler<ScreenEventArgs> handler = ShowScreenEvent;
            if (handler != null)
            {
                handler(this, e);
            }
            TryClose();
        }
    }
}
