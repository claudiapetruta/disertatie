﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using MessagingProtoClient.Events;

namespace MessagingProtoClient.ViewModels
{
    class NotificationPopUpViewModel : Screen, IShowScreen
    {
        private string message;

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value; NotifyOfPropertyChange(() => Message);
            }
        }

        public NotificationPopUpViewModel(string newMessage)
        {
            Message = newMessage;

        }

    }
}
