﻿using Caliburn.Micro;
using CommonClasses.CommonEnums;
using CommonClasses.CommonUtils;
using CommonClasses.CommonModels;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Models;
using MessagingProtoClient.Utils;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System;
using MessagingProtoClient.Events;

namespace MessagingProtoClient.ViewModels
{
    [Export(typeof(LoginViewModel))]
    public class LoginViewModel : Screen,IShowScreen
    {
        ClientConnectionHandler connection;
        public string Username { get; set; }
        public string Password { get; set; }
        private string error;

        public string Error
        {
            get { return error; }
            set
            {
                error = value;
                NotifyOfPropertyChange(() => Error);
            }
        }

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public LoginViewModel(ClientConnectionHandler con)
        {
            connection= con;
        }

        public void SetPassword(PasswordBox box)
        {
            Password = box.Password;
        }

        public void OpenRegistration()
        {
            ChangeScreenEventMethod(new RegistrationViewModel(connection));
        }

        public void Login()
        {
            RequestEncoder encoder = new RequestEncoder();
            
            string passwordHash = PasswordHashing.GeneratePasswordHash(Password);
            LoginModel loginDetails = new LoginModel(Username, passwordHash);

            CurrentUser.Username = Username;

            string infoToSend = encoder.EncodeRequest(RequestTypes.Login, loginDetails);

            string serverResponse = connection.SendReceiveInformation(infoToSend);

            if (serverResponse == "True")
            {
                ChangeScreenEventMethod(new HomeViewModel(connection));
            }
            else
            {
                Error = "Your username or password is incorrect!";
            }

        }

        protected virtual void ChangeScreenEventMethod(IShowScreen newScreen)
        {
            ScreenEventArgs e = new ScreenEventArgs(this,newScreen,true);
            
            EventHandler<ScreenEventArgs> handler = ShowScreenEvent;
            if (handler != null)
            {
                handler(this, e);
            }
            TryClose();
        }
    }
}
