﻿using Caliburn.Micro;
using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using CommonClasses.CommonUtils;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Events;
using MessagingProtoClient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;

namespace MessagingProtoClient.ViewModels
{
    [Export(typeof(RegistrationViewModel))]
    class HomeViewModel : Screen,IShowScreen
    {
        #region HomeViewModel attributes

        public ClientConnectionHandler connection;
        private UIFriendModel selectedFriend;
        private FriendsModel selectedPendingRequest;
        private List<UIFriendModel> uiFriendsList =new List<UIFriendModel>();
        private List<FriendsModel> pendingList = new List<FriendsModel>();
        private string deleteAccount;
        private string viewBlockedList;
        private Thread thread;

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;

        public List<UIFriendModel> FriendsList { get; set; }

        public string ViewBlockedList
        {
            get
            {
                return viewBlockedList;
            }
            set
            {
                viewBlockedList = value;
                NotifyOfPropertyChange(() => ViewBlockedList);
            }
        }


        public List <FriendsModel> PendingList
        {
            get
            {
                return pendingList;
            }
            set
            {
                pendingList = value;
                NotifyOfPropertyChange(() => PendingList);
            }
        }

        public UIFriendModel SelectedFriend
        {
            get
            {
                return selectedFriend;
            }
            set
            {
                //FriendToShow
                selectedFriend = value;
                ShowMessageWindow(selectedFriend.FriendToShow);
                NotifyOfPropertyChange(() => SelectedFriend);
            }
        }

        public FriendsModel SelectedPendingRequest
        {
            get
            {
                return selectedPendingRequest;
            }
            set
            {
                selectedPendingRequest = value;
                ShowReplyPendingWindow(selectedPendingRequest.InitiatorUser);
                NotifyOfPropertyChange(() => SelectedPendingRequest);
            }
        }

        public List<UIFriendModel> UIFriendsList
        {
            get
            {
                return uiFriendsList;
            }
            set
            {
                uiFriendsList = value;
                NotifyOfPropertyChange(() => UIFriendsList);
            }
        }
        #endregion

        #region Constructors

        [ImportingConstructor]
        public HomeViewModel(ClientConnectionHandler con)
        {
            connection = con;

            connection.requestDecoder.GetPendingListReply += GetPendingListReplyCallback;
            connection.requestDecoder.GetFriendListReply += GetFriendListReplyCallback;

            thread = new Thread(new ParameterizedThreadStart(connection.StartReceiveRequest));
            thread.Start();
            
            SendPendingListRequest(CurrentUser.Username);
            GetFriends(CurrentUser.Username);
        }

        #endregion

        #region Window Opener Events

        private void ShowMessageWindow(string value)
        {
            if (value != null)
            {
                ShowScreenEventMethod(new MessageViewModel(value,connection));
            }
        }

        private void ShowBlockedListWindow(string value)
        {
            if (value != null)
            {
                ShowScreenEventMethod(new BlockedUsersViewModel());
            }
        }

        private void ShowReplyPendingWindow(string selectedValue)
        {
            if (selectedValue != null)
            {
                FriendsModel tempFriend = PendingList.Single(s => s.InitiatorUser == selectedValue);
                FriendReplyModel friendData = new FriendReplyModel();
                friendData.Id = tempFriend.Id;
                ShowScreenEventMethod(new ReplyPendingViewModel(connection, friendData));
            }
        }

        public void ShowAddFriendWindow()
        {
            ShowScreenEventMethod(new AddFriendViewModel(connection));
        }
        #endregion

        #region Private Methods

        private void GetFriends(string username)
        {
            List<UIFriendModel> friendsList = new List<UIFriendModel>();
            RequestEncoder encoder = new RequestEncoder();

            string infoToSend = encoder.EncodeRequest(RequestTypes.GetFriendsList, username);
            connection.SendInformation(infoToSend);
        }

        private void SendPendingListRequest(string username)
        {
            RequestEncoder encoder = new RequestEncoder();
            string infoToSend = encoder.EncodeRequest(RequestTypes.GetPendingList, username);
            connection.SendInformation(infoToSend);
        }

        #endregion


        /// <summary>
        /// shows a new screen and closes the old screen
        /// </summary>
        /// <param name="newScreen"></param>
        protected virtual void ChangeScreenEventMethod(IShowScreen newScreen)
        {
            ScreenEventArgs e = new ScreenEventArgs(this, newScreen, true);

            EventHandler<ScreenEventArgs> handler = ShowScreenEvent;
            if (handler != null)
            {
                handler(this, e);
            }
            TryClose();
        }

        /// <summary>
        /// shows a new screen without closing the old screen
        /// </summary>
        /// <param name="newScreen"></param>
        protected virtual void ShowScreenEventMethod(IShowScreen newScreen)
        {
            ScreenEventArgs e = new ScreenEventArgs(this, newScreen, false);

            EventHandler<ScreenEventArgs> handler = ShowScreenEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        public void GetPendingListReplyCallback(object sender, GenericEventArgs e)
        {
            PendingList = (List<FriendsModel>)e.GenericArg;
        }

        public void GetFriendListReplyCallback(object sender, GenericEventArgs e)
        {
            UIFriendsList = (List<UIFriendModel>)e.GenericArg;
        }
    }
}
