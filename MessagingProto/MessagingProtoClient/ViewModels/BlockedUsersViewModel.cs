﻿using Caliburn.Micro;
using MessagingProtoClient.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.ViewModels
{
    class BlockedUsersViewModel:Screen, IShowScreen
    {
        private readonly IWindowManager windowManager;

        public BlockedUsersViewModel()
        {
        }

        public BlockedUsersViewModel(IWindowManager theWindowManager)
        {
            windowManager = theWindowManager;
        }

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;
    }
}
