﻿using Caliburn.Micro;
using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using CommonClasses.CommonUtils;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Events;
using MessagingProtoClient.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.ViewModels
{
    class MessageViewModel:Screen, IShowScreen
    {
        private string messageInput;

        ClientConnectionHandler connection;

        private string UserNameDest;

        public ObservableCollection<string> showMessages;

        public ObservableCollection<string> ShowMessages
        {
            get { return showMessages; }
            set
            {
                showMessages = value;
                NotifyOfPropertyChange(() => showMessages);
            }
        }

        public string MessageInput
        {
            get { return messageInput; }
            set
            {
                messageInput = value;
                NotifyOfPropertyChange(() => messageInput);
            }
        }

        public MessageViewModel(string userNameDest, ClientConnectionHandler conn )
        {
            UserNameDest = userNameDest;
            connection = conn;
            connection.requestDecoder.MessageReceive += ReceiveMessageCallback;
            ShowMessages = new ObservableCollection<string>();
        }

        public void SendMessage()
        {

            RequestEncoder encoder = new RequestEncoder();

            MessagesModel messageToSend = new MessagesModel(UserNameDest, "", messageInput);

            string infoToSend = encoder.EncodeRequest(RequestTypes.SendMessage, messageToSend);

            connection.SendInformation(infoToSend);

            MessageInput = "";

            //Console.WriteLine("message trimis to {0}:{1}", UserNameDest, messageInput);
        }

        public event EventHandler<ScreenEventArgs> ShowScreenEvent;
        public void ReceiveMessageCallback(object sender, GenericEventArgs e)
        {
            MessagesModel receiveMessage = (MessagesModel)e.GenericArg;

            string destName = receiveMessage.Destination;
            string senderName = receiveMessage.Sender;
            if (UserNameDest != destName && UserNameDest != senderName )
            {
                return;
            }
            if (CurrentUser.Username != destName && CurrentUser.Username != senderName)
            {
                return;
            }


            string finalMessage = receiveMessage.MessageTimeStamp + " " + senderName + ": " + receiveMessage.MessageBody;

            Console.WriteLine(finalMessage);

            App.Current.Dispatcher.Invoke(new System.Action(() => {
                ShowMessages.Add(finalMessage);
            }));

            

            //timestamp nume message

        }
    }
}
