﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoClient.ViewModels
{
    [Export(typeof(LoadingViewModel))]
    public class LoadingViewModel : Screen
    {
        private string loadingErrorMessage;

        [ImportingConstructor]
        public LoadingViewModel(IWindowManager windowManager)
        {
            UIController uiController = new UIController(windowManager);
            //this.windowManager = windowManager;
            Console.WriteLine(windowManager.GetHashCode());
            
        }

        private void CreateUiController(object nullparam)
        {

        }
        public string LoadingErroMessage
        {
            get { return loadingErrorMessage; }
            set
            {
                loadingErrorMessage = value;
                NotifyOfPropertyChange(() => LoadingErroMessage);
            }
        }

    }
}
