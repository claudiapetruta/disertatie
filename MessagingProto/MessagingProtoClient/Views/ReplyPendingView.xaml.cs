﻿using System.Windows;

namespace MessagingProtoClient.Views
{
    /// <summary>
    /// Interaction logic for ReplyPendingView.xaml
    /// </summary>
    public partial class ReplyPendingView : Window
    {
        public ReplyPendingView()
        {
            InitializeComponent();
        }
    }
}
