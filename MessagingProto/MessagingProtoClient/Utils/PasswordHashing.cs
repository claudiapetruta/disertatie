﻿using System.Security.Cryptography;
using System.Text;


namespace MessagingProtoClient.Utils
{
    class PasswordHashing
    {
        public static string GeneratePasswordHash(string password)
        {
            string resultingHash;
            SHA512 sha512 = SHA512.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(password);
            byte[] hash = sha512.ComputeHash(bytes);
            resultingHash = Encoding.ASCII.GetString(hash);
            return resultingHash;
        }
    }
}
