﻿using Caliburn.Micro;
using MessagingProtoClient.CommunicationManager;
using MessagingProtoClient.Events;
using MessagingProtoClient.ViewModels;
using System.Windows;

namespace MessagingProtoClient
{
    public class UIController
    {
        private readonly IWindowManager windowManager;

        public ClientConnectionHandler connection;

        public UIController(IWindowManager windowManager)
        {
            this.windowManager = windowManager;
            connection = new ClientConnectionHandler();
            connection.StartConnection("127.0.0.1");
            IShowScreen loginScreen = new LoginViewModel(connection);
            loginScreen.ShowScreenEvent += ShowScreenCallback;
            windowManager.ShowWindow(loginScreen);
        }

        public void ShowScreenCallback(object sender, ScreenEventArgs e)
        {
            e.NewScreen.ShowScreenEvent += ShowScreenCallback;

            Application.Current.Dispatcher.Invoke(() =>
            {
                windowManager.ShowWindow(e.NewScreen);
                
            });
            if (e.CloseOldScreen)
            {
                e.OldScreen.ShowScreenEvent -= ShowScreenCallback;
                ((Screen)(e.OldScreen)).TryClose();
            }

        }
    }
}
