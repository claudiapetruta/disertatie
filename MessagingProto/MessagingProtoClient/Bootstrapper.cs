﻿using Caliburn.Micro;
using MessagingProtoClient.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Primitives;

namespace MessagingProtoClient
{
    class Bootstrapper : BootstrapperBase
    {
        private readonly IWindowManager windowManager;
        public Bootstrapper()
        {
            windowManager = new WindowManager();
            Initialize();
        }

        private CompositionContainer container;

       // private readonly SimpleContainer _container = new SimpleContainer();

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            new UIController(windowManager);
            //DisplayRootViewFor<LoadingViewModel>();
        }

        protected override void Configure()
        {
            //_container.Singleton<IEventAggregator, EventAggregator>();
            container = new CompositionContainer(new AggregateCatalog(AssemblySource.Instance.Select(x => 
                new AssemblyCatalog(x)).OfType<ComposablePartCatalog>()));

            CompositionBatch batch = new CompositionBatch();
            Console.WriteLine("config");
            
            batch.AddExportedValue<IWindowManager>(windowManager);
            //batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue(container);

            container.Compose(batch);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            string contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            var exports = container.GetExportedValues<object>(contract);

            if (exports.Count() > 0)
            {
                return exports.First();
            }

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", contract));
        }
    }
}
