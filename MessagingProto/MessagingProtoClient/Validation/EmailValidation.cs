﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MessagingProtoClient.Validation
{
    class EmailValidation
    {
        public static bool ValidateEmailFormat(string emailFormat)
        {
            if(Regex.IsMatch(emailFormat, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                return true;
            }
            return false;
        }
    }
}
