﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoClient.Events;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MessagingProtoClient.CommunicationManager
{
    
    public class ReplyDecoder
    {
        public event EventHandler<GenericEventArgs> GetPendingListReply;
        public event EventHandler<GenericEventArgs> GetFriendListReply;
        public event EventHandler<GenericEventArgs> AddFriendsReply;
        public event EventHandler<GenericEventArgs> DeleteFriendsReply;
        public event EventHandler<GenericEventArgs> MessageReceive;

        public void DecodeRequest(object requestContent)
        {
            string message;
            
            dynamic data = JsonConvert.DeserializeObject(requestContent.ToString());
            string receivedData;
            Console.WriteLine("The code is: {0}, {1}", data.RequestType, (RequestTypes)data.RequestType);

            switch ((RequestTypes)data.RequestType)
            {
                case RequestTypes.AddFriendReply:
                    receivedData = JsonConvert.DeserializeObject(data.SerializedJson.ToString());

                    if (receivedData == "True")
                    {
                        message = "Friend request sent";
                    }
                    else
                    {
                        message = "Friend request couldn't be sent";
                    }

                    AddFriendsReplyEventMethod(message);

                    Console.WriteLine("The server response for the AddFriend request is: ", data.SerializedJson);


                    break;
                case RequestTypes.GetOnlineStatus:
                    Console.WriteLine("The server response for the GetStatus request is: ", data.SerializedJson);

                    break;

                case RequestTypes.ChangeFriendshipStatusReply:
                    { 

                        receivedData = JsonConvert.DeserializeObject(data.SerializedJson.ToString());

                        Console.WriteLine(">>Server response for Change friendship status request is: {0}", receivedData);
                        if (receivedData == RelationshipStatus.Accepted.ToString())
                        {
                            message = "You've accepted the friend request.";
                        }
                        else if (receivedData == RelationshipStatus.Blocked.ToString())
                        {
                            message = "You've blocked the user that sent you the request.";
                        }
                        else
                        {
                            message = "Your response couldn't be processed, please try again.";
                        }
            }
                    break;

                case RequestTypes.NotifyUpdatePendingList:
                    {
                        List<FriendsModel> pendingList = new List<FriendsModel>();
                        pendingList = JsonConvert.DeserializeObject<List<FriendsModel>>(data.SerializedJson.ToString());

                        //model.ReceivePendingList(pendingList);
                        break;
                    }

                case RequestTypes.GetFriendsListReply:
                    {
                        List<UIFriendModel> friendList = new List<UIFriendModel>(); 
                        friendList = JsonConvert.DeserializeObject<List<UIFriendModel>>(data.SerializedJson.ToString());
                        
                        GetFriendsListReplyEventMethod(friendList);

                        break;
                    }

                case RequestTypes.GetPendingListReply:
                    {
                        List<FriendsModel> pendingList = new List<FriendsModel>();
                        pendingList = JsonConvert.DeserializeObject<List<FriendsModel>>(data.SerializedJson.ToString());

                        GetPendingListReplyEventMethod(pendingList);
                        break;
                    }

                case RequestTypes.DeleteFriendshipReply:
                    {
                        receivedData = JsonConvert.DeserializeObject(data.SerializedJson.ToString());

                        if (receivedData == "True")
                        {
                            message = "Friendship deleted";
                        }
                        else
                        {
                            message = "Friendship couldn't be deleted";
                        }

                        DeleteFriendsReplyEventMethod(message);

                        Console.WriteLine("The server response for the DeleteFriend request is: ", data.SerializedJson);
                        break;
                    }

                case RequestTypes.SendMessageReply:
                case RequestTypes.ReceiveMessage:
                    {
                        MessagesModel messageSent = JsonConvert.DeserializeObject<MessagesModel>(data.SerializedJson.ToString());

                        ReceiveMessageEventMethod(messageSent);

                        //Console.WriteLine("Receive message. typre request:{0}", (RequestTypes)data.RequestType);
                        //Console.WriteLine("dest:{0}\nsender:{1}\nmessage:{2}", messageSent.Destination,messageSent.Sender,messageSent.MessageBody);

                        break;
                    }

                default:
                    Console.WriteLine("Something went wrong. The request couldn't be parsed");
                    break;
            }
        }
        protected virtual void GetPendingListReplyEventMethod(object genericArg)
        {
            GenericEventArgs e = new GenericEventArgs();
            e.GenericArg = genericArg;

            EventHandler<GenericEventArgs> handler = GetPendingListReply;
            if (handler != null)
            {
                handler(this, e);
            } 
        }

        protected virtual void GetFriendsListReplyEventMethod(object genericArg)
        {
            GenericEventArgs e = new GenericEventArgs();
            e.GenericArg = genericArg;

            EventHandler<GenericEventArgs> handler = GetFriendListReply;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void AddFriendsReplyEventMethod(object genericArg)
        {
            GenericEventArgs e = new GenericEventArgs();
            e.GenericArg = genericArg;

            EventHandler<GenericEventArgs> handler = AddFriendsReply;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void DeleteFriendsReplyEventMethod(object genericArg)
        {
            GenericEventArgs e = new GenericEventArgs();
            e.GenericArg = genericArg;

            EventHandler<GenericEventArgs> handler = DeleteFriendsReply;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void ReceiveMessageEventMethod(object genericArg)
        {
            GenericEventArgs e = new GenericEventArgs();
            e.GenericArg = genericArg;

            EventHandler<GenericEventArgs> handler = MessageReceive;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
