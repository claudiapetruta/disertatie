﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace MessagingProtoClient.CommunicationManager
{
    public class ClientConnectionHandler
    {
        public ReplyDecoder requestDecoder = new ReplyDecoder();
        private TcpClient clientSocket = new TcpClient();
        private int port = 5373;
        private bool listeningServerRequest = false;
        public void StartConnection(string ipAddress)
        {
            if(!clientSocket.Connected)
            {
                clientSocket.Connect(ipAddress, port);
                listeningServerRequest = true;
            }
        }

        public string SendReceiveInformation(string infoToSend)
        {
            byte[] byteArray;
            byte[] size2 = new byte[sizeof(int) + 1];
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] text = Encoding.ASCII.GetBytes(infoToSend);
            byteArray = BitConverter.GetBytes(text.Length);
            serverStream.Write(byteArray, 0, sizeof(int));
            serverStream.Flush();
            serverStream.Write(text, 0, text.Length);
            serverStream.Flush();
            serverStream.Read(size2, 0, sizeof(int));
            int rez = BitConverter.ToInt32(size2, 0);
            byte[] inStream = new byte[rez];
            serverStream.Read(inStream, 0, rez);
            string returnData = Encoding.ASCII.GetString(inStream);
            return returnData;
        }

        public void SendInformation(string infoToSend)
        {
            byte[] byteArray;
            byte[] size2 = new byte[sizeof(int) + 1];
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] text = Encoding.ASCII.GetBytes(infoToSend);
            byteArray = BitConverter.GetBytes(text.Length);
            serverStream.Write(byteArray, 0, sizeof(int));
            serverStream.Flush();
            serverStream.Write(text, 0, text.Length);
            serverStream.Flush();
        }

        public void StartReceiveRequest(object obj)
        {
            while (listeningServerRequest)
            {
                string dataFromClient;
                
                IPEndPoint remoteIpEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;
                NetworkStream networkStream = clientSocket.GetStream();
                byte[] bytesFrom = new byte[10025];
                byte[] size = new byte[sizeof(int) + 1];

                networkStream.Read(size, 0, sizeof(int));
      
                int rez = BitConverter.ToInt32(size, 0);

                networkStream.Read(bytesFrom, 0, rez);
                dataFromClient = Encoding.ASCII.GetString(bytesFrom);
                dataFromClient = dataFromClient.Substring(0, rez);

                Thread thread = new Thread(new ParameterizedThreadStart(requestDecoder.DecodeRequest));
                
                thread.Start(dataFromClient);
            }
        }
    }
}
