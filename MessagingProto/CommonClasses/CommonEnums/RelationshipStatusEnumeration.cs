﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonEnums
{
    public enum RelationshipStatus
    {
        Pending,
        Accepted,
        Blocked
    }
}
