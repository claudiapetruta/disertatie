﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.Logging
{
    public class LoggingManager
    {
        public static string CreateLoggingFile()
        {
            string logFile = "..\\..\\Repository\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            if (!System.IO.File.Exists(logFile))
            {
                System.IO.File.Create(logFile);
            }
            return logFile;
        }
        public static void LogInformation(string stringToLog)
        {
            string file = CreateLoggingFile();
            string logger = DateTime.Now.ToString("HH:mm:ss") + " " + stringToLog;
            System.IO.File.AppendAllText(file, logger + Environment.NewLine);
        }
    }
}
