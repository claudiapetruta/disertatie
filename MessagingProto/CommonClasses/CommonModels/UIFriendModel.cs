﻿using CommonClasses.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class UIFriendModel
    {
        public int FriendshipId { get; set; }
        public string FriendToShow { get; set; }
        public int OfflineMessagesNumber { get; set; }
        public OnlineStatus FriendOnlineStatus { get; set; }

        public UIFriendModel() { }

        public UIFriendModel(int id, string friend)
        {
            FriendshipId = id;
            FriendToShow = friend;
        }
    }
}
