﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class RequestStatusEnumeration
    {
        public enum RequestStatus
        {
            Pending,
            Accepted,
            Denied,
            Blocked
        }
    }
}
