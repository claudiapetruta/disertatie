﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class MessagesModel
    {
        public int Id { get; set; }
        public string Destination { get; set; }
        public string Sender { get; set; }
        public string MessageBody { get; set; }
        public string MessageTimeStamp { get; set; }

        public MessagesModel()
        {

        }
        public MessagesModel(string DestinationUser, string SenderUser, string Message)
        {
            Destination = DestinationUser;
            Sender = SenderUser;
            MessageBody = Message;
        }
    }
}
