﻿using CommonClasses.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class FriendReplyModel
    {
        public int Id { get; set; }
        public RelationshipStatus Status { get; set; }

        public FriendReplyModel() { }
        public FriendReplyModel(int id, RelationshipStatus status)
        {
            Id = id;
            Status = status;
        }
    }
}
