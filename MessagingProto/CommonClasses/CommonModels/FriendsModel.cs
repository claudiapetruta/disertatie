﻿using CommonClasses.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class FriendsModel
    {
        public int Id { get; set; }
        public string InitiatorUser { get; set; }
        public string TargetUser { get; set; }
        public RelationshipStatus RequestStatus { get; set; }

        public FriendsModel() { }

        public FriendsModel(string initiatorUser, string targetUser, RelationshipStatus requestStatus)
        {
            InitiatorUser = initiatorUser;
            TargetUser = targetUser;
            RequestStatus = requestStatus;
        }
    }
}