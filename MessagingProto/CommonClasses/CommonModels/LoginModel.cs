﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string PasswordHash { get; set; }

        public LoginModel(string username, string passwordHash)
        {
            Username = username;
            PasswordHash = passwordHash;
        }
    }
}
