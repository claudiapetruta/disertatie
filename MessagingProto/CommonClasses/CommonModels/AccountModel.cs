﻿using CommonClasses.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses.CommonModels
{
    public class AccountModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public AccountModel() { }
        public AccountModel(string username, string password, string email)
        {
            Username = username;
            Password = password;
            Email = email;
        }
    }
}
