﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DBManagement
{
    class TableCreation
    {
        public void CreateTables()
        {
            DBConnection dbConn = new DBConnection();
            SQLiteConnection connect = dbConn.OpenDBConnection();
            SQLiteCommand command;
            string dropTable;

            dropTable = "DROP TABLE IF EXISTS friends";
            command = new SQLiteCommand(dropTable, connect);
            command.ExecuteNonQuery();

            string friends = "CREATE TABLE friends (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "initiator_user VARCHAR(50), " +
                "target_user VARCHAR(50), " +
                "request_status VARCHAR(10)," +
                "FOREIGN KEY (initiator_user) REFERENCES accounts(username), " +
                "FOREIGN KEY (target_user) REFERENCES accounts(username))";
            command = new SQLiteCommand(friends, connect);
            command.ExecuteNonQuery();

            dropTable = "DROP TABLE IF EXISTS accounts";
            command = new SQLiteCommand(dropTable, connect);
            command.ExecuteNonQuery();

            string users = "CREATE TABLE accounts (" +
                "username VARCHAR(50) PRIMARY KEY, " +
                "password VARCHAR(300) NOT NULL, " +
                "email VARCHAR(50) NOT NULL)";
            command = new SQLiteCommand(users, connect);
            command.ExecuteNonQuery();

            dropTable = "DROP TABLE IF EXISTS messages";
            command = new SQLiteCommand(dropTable, connect);
            command.ExecuteNonQuery();

            string messages = "CREATE TABLE messages (" +
                "id INT PRIMARY KEY, " +
                "destination VARCHAR(50) NOT NULL, " +
                "sender VARCHAR(50) NOT NULL, " +
                "content VARCHAR(1000), " +
                "timestamp VARCHAR(30) NOT NULL, " +
                "FOREIGN KEY(destination) REFERENCES accounts(username), " +
                "FOREIGN KEY(sender) REFERENCES accounts(username))";
                
            command = new SQLiteCommand(messages, connect);
            command.ExecuteNonQuery();

            connect.Close();
        }
    }
}
