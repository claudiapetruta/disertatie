﻿using CommonClasses.CommonModels;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DBManagement
{
    class MessagesDBManagement
    {
        public List<MessagesModel> GetMessagesFromDB(string sender, string receiver)
        {
            MessagesModel message = new MessagesModel();
            List<MessagesModel> messageList = new List<MessagesModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM messages WHERE (sender=@Sender AND destination=@Receiver)" +
                "OR (sender=@Receiver AND destination=@Sender) ORDER BY timestamp", connection);
            getSQL.Parameters.AddWithValue("@Sender", sender);
            getSQL.Parameters.AddWithValue("@Sender", receiver);
            SQLiteDataReader reader = getSQL.ExecuteReader();
            while (reader.Read())
            {
                message.Sender = reader["sender"].ToString();
                message.Destination = reader["destiation"].ToString();
                message.MessageBody = reader["content"].ToString();
                message.MessageTimeStamp = reader["timestamp"].ToString();

                messageList.Add(message);
            }

            connection.Close();

            return messageList;
        }

        public void AddToDB(MessagesModel message)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO messages (destination, sender, content, timestamp) " +
                "VALUES (@Destination, @Sender,@Content,@Timestamp)", connection);
            insertSQL.Parameters.AddWithValue("@Destination", message.Destination);
            insertSQL.Parameters.AddWithValue("@Sender", message.Sender);
            insertSQL.Parameters.AddWithValue("@Content", message.MessageBody);
            insertSQL.Parameters.AddWithValue("@Timestamp", message.MessageTimeStamp);
            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }
    }
}
