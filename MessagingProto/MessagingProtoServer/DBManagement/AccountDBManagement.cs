﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;

namespace MessagingProtoServer.DBManagement
{
    class AccountDBManagement
    {
        public AccountModel GetAccountFromDB(string username)
        {
            AccountModel account = new AccountModel();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM accounts WHERE username=@Username", connection);
            getSQL.Parameters.AddWithValue("@Username", username);

            SQLiteDataReader reader = getSQL.ExecuteReader();
            while (reader.Read())
            {
                account.Username = reader["username"].ToString();
                account.Password = reader["password"].ToString();
                account.Email = reader["email"].ToString();
            }

            connection.Close();

            return account;
        }

        public bool CheckUsernameUnique(string username)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT count(*) FROM accounts WHERE username=@Username", connection);
            getSQL.Parameters.AddWithValue("@Username", username);

            int count = Convert.ToInt32(getSQL.ExecuteScalar());

            if (0 == count)
            {
                return true;
            }

            return false;
        }

        public bool CheckEmailUnique(string email)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM accounts WHERE email=@Email", connection);
            getSQL.Parameters.AddWithValue("@Email", email);

            int count = Convert.ToInt32(getSQL.ExecuteScalar());

            if (0 == count)
            {
                return true;
            }

            return false;
        }

        public List<AccountModel> GetAllAccountsFromDB()
        {
            AccountModel account = new AccountModel();
            List<AccountModel> userList = new List<AccountModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM accounts", connection);

            SQLiteDataReader reader = getSQL.ExecuteReader();

            while (reader.Read())
            {
                account.Username = reader["username"].ToString();
                account.Password = reader["password"].ToString();
                account.Email = reader["email"].ToString();
                userList.Add(account);
            }

            connection.Close();

            return userList;
        }

        public bool CheckPasswordMatches(string username, string password)
        {
            string result = null;
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT password FROM accounts WHERE " +
                "username=@Username AND password=@Password", connection);
            getSQL.Parameters.AddWithValue("@Username", username);
            getSQL.Parameters.AddWithValue("@Password", password);

            SQLiteDataReader reader = getSQL.ExecuteReader();
            while (reader.Read())
            {
                result = reader["password"].ToString();
            }

            connection.Close();

            if (! string.IsNullOrEmpty(result))
            {
                return true;
            }

            return false;
        }

        public void SetAccountPassword(string username, string password)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand updateSQL = new SQLiteCommand("UPDATE accounts SET password=@Password WHERE username=@Username", connection);
            updateSQL.Parameters.AddWithValue("@Password", password);
            updateSQL.Parameters.AddWithValue("@Username", username);

            try
            {
                updateSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        public void SetAccountIpAddress(string username, string ipAddress)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand updateSQL = new SQLiteCommand("UPDATE accounts SET ipaddress=@Ipaddress WHERE username=@Username", connection);
            updateSQL.Parameters.AddWithValue("@Ipaddress", ipAddress);
            updateSQL.Parameters.AddWithValue("@Username", username);

            try
            {
                updateSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        public void AddAccountToDB(AccountModel account)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO accounts (username, password, email) " +
                "VALUES (@Username, @Password, @Email)", connection);
            insertSQL.Parameters.AddWithValue("@Username", account.Username);
            insertSQL.Parameters.AddWithValue("@Password", account.Password);
            insertSQL.Parameters.AddWithValue("@Email", account.Email);
            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        public void DeleteFromDB(string username)
        {
            DBConnection dbConn = new DBConnection();
            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand deleteSQL = new SQLiteCommand("DELETE FROM accounts WHERE username=@Username", connection);
            deleteSQL.Parameters.AddWithValue("@Username", username);
            try
            {
                deleteSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        
    }
}
