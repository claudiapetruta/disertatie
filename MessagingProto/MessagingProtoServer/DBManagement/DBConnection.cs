﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DBManagement
{
    class DBConnection
    {
        SQLiteConnection dbConnection;
        string dbPath = System.Environment.CurrentDirectory + "..\\..\\..\\..\\ProtoDatabase";

        public void CreateDbFile()
        {
            if (!string.IsNullOrEmpty(dbPath) && !Directory.Exists(dbPath))
            {
                Directory.CreateDirectory(dbPath);
            }

            string dbFilePath = dbPath + "\\MessagingProtoDB.db";

            if (!File.Exists(dbFilePath))
            {
                SQLiteConnection.CreateFile(dbFilePath);
            }
        }
        public SQLiteConnection OpenDBConnection()
        {
            string strCon = string.Format("Data Source={0};", dbPath + "\\MessagingProtoDB.db");
            dbConnection = new SQLiteConnection(strCon);
            dbConnection.Open();
            return dbConnection;
   
        }
    }
}
