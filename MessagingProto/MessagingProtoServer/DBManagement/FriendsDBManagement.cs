﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace MessagingProtoServer.DBManagement
{
    class FriendsDBManagement
    {
        public List<FriendsModel> GetFriendListFromDB(string username)
        {
            List<FriendsModel> friendsList = new List<FriendsModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM friends WHERE (initiator_user=@Username OR " +
                "target_user=@Username) AND request_status='Accepted'", connection);
            getSQL.Parameters.AddWithValue("@Username", username);

            SQLiteDataReader reader = getSQL.ExecuteReader();
            Console.WriteLine("Database friend list: ");
            while (reader.Read())
            {
                FriendsModel friend = new FriendsModel();
                friend.Id = Convert.ToInt32(reader["id"]);
                friend.InitiatorUser = reader["initiator_user"].ToString();
                friend.TargetUser = reader["target_user"].ToString();
                friend.RequestStatus =(RelationshipStatus)Enum.Parse(typeof(RelationshipStatus), reader["request_status"].ToString());

                friendsList.Add(friend);
            }

            connection.Close();

            return friendsList;
        }

        public List<FriendsModel> GetPendingRequestsFromDB(string username)
        {
            List<FriendsModel> pendingList = new List<FriendsModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM friends WHERE " +
                "target_user=@Username AND request_status='Pending'", connection);
            getSQL.Parameters.AddWithValue("@Username", username);

            SQLiteDataReader reader = getSQL.ExecuteReader();

            Console.WriteLine("Database pending list: ");
            while (reader.Read())
            {
                FriendsModel friend = new FriendsModel();
                friend.Id = Convert.ToInt32(reader["id"]);
                friend.InitiatorUser = reader["initiator_user"].ToString();
                friend.TargetUser = reader["target_user"].ToString();
                friend.RequestStatus = (RelationshipStatus)Enum.Parse(typeof(RelationshipStatus), reader["request_status"].ToString());

                Console.WriteLine("Initiator: {0}, Target: {1}, Request: {2}", friend.InitiatorUser, friend.TargetUser, friend.RequestStatus);
                pendingList.Add(friend);
            }

            connection.Close();

            return pendingList;
        }

        public List<FriendsModel> GetBlockedUsersFromDB(string username)
        {
            List<FriendsModel> blockedList = new List<FriendsModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT * FROM friends WHERE (initiator_user=@Username OR " +
                "target_user=@Username) AND request_status='Blocked'", connection);
            getSQL.Parameters.AddWithValue("@Username", username);

            SQLiteDataReader reader = getSQL.ExecuteReader();

            while (reader.Read())
            {
                FriendsModel friend = new FriendsModel();
                friend.Id = Convert.ToInt32(reader["id"]);
                friend.InitiatorUser = reader["initiator_user"].ToString();
                friend.TargetUser = reader["target_user"].ToString();
                friend.RequestStatus = (RelationshipStatus)Enum.Parse(typeof(RelationshipStatus), reader["request_status"].ToString());

                blockedList.Add(friend);
            }

            connection.Close();

            return blockedList;
        }
        public void AddFriendToDB(string initiatorUsername, string targertUsername, RelationshipStatus requestStatus)
        {
            DBConnection dbConn = new DBConnection();
            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO friends (initiator_user, target_user, request_status) VALUES (@InitiatorUser, @TargetUser, @RequestStatus)", connection);
            insertSQL.Parameters.AddWithValue("@InitiatorUser", initiatorUsername);
            insertSQL.Parameters.AddWithValue("@TargetUser", targertUsername);
            insertSQL.Parameters.AddWithValue("@RequestStatus", requestStatus.ToString());
            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        public void DeleteFriendshipFromDB(int id)
        {
            DBConnection dbConn = new DBConnection();
            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand deleteSQL = new SQLiteCommand("DELETE FROM friends WHERE id=@Id", connection);
            deleteSQL.Parameters.AddWithValue("@Id", id);
            try
            {
                deleteSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            connection.Close();
        }

        public bool UpdateFriendshipStatus(int id, RelationshipStatus requestStatus)
        {
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand updateSQL = new SQLiteCommand("UPDATE friends SET request_status=@RequestStatus WHERE id=@Id", connection);
            updateSQL.Parameters.AddWithValue("@Id", id);
            updateSQL.Parameters.AddWithValue("@RequestStatus", requestStatus.ToString());

            try
            {
                updateSQL.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                connection.Close();
                return false;
            }
        }

        public bool CheckIfFriendshipExistsDB(string username, string friend)
        {   
            List<FriendsModel> friendsList = new List<FriendsModel>();
            DBConnection dbConn = new DBConnection();

            SQLiteConnection connection = dbConn.OpenDBConnection();
            SQLiteCommand getSQL = new SQLiteCommand("SELECT COUNT(*) FROM friends WHERE (initiator_user=@Initiator OR initiator_user = @Target) AND (target_user=@Initiator OR target_user = @Target)", connection);
            getSQL.Parameters.AddWithValue("@Initiator", username);
            getSQL.Parameters.AddWithValue("@Target", friend);
            int reader = Convert.ToInt32(getSQL.ExecuteScalar());

            if (reader > 0)
            {
                connection.Close();
                return true;
            }
           
            connection.Close();
            return false;
        }
    }
}
