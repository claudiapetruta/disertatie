﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.CommunicationSystem
{
    public class RequestParameter
    {
        public string ReceivedData { get; set; }
        public TcpClient ClientSocket { get; set; }
        
        public RequestParameter(string data, TcpClient socket)
        {
            ReceivedData = data;
            ClientSocket = socket;
        }
    }
}
