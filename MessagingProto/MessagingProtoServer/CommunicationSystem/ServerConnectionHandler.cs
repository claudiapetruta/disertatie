﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using MessagingProtoServer.BusinessLogic;

namespace MessagingProtoServer.CommunicationSystem
{
    class ServerConnectionHandler
    {
        TcpListener serverSocket;
       
        Boolean listening = true;
        RequestHandler requestHandler = new RequestHandler();
        public ServerConnectionHandler()
        {
            requestHandler.handler.SendRequestEvent += SendRequestEvent;
            serverSocket = new TcpListener(IPAddress.Any, 5373);
        }

        public void StartListening()
        {
            TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            Console.WriteLine(">> Server Started");
            IPEndPoint remoteIpEndPoint;
            while (listening)
            {
                clientSocket = serverSocket.AcceptTcpClient();
                remoteIpEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;
                Console.WriteLine(remoteIpEndPoint.Address + " was connected.");

                Thread thread = new Thread(new ParameterizedThreadStart(ManageClient));
                thread.Start(clientSocket);
            }
            
       
        }
        public void ManageClient(object clientSockObj)
        {
            string data;
            RequestParameter reqParam;
            
            TcpClient clientSocket = (TcpClient)clientSockObj;
            IPEndPoint remoteIpEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;

            Console.WriteLine(">> Thread started for " + remoteIpEndPoint.Address);

            while(ReceiveInfo(clientSocket, out data))
            {
                reqParam = new RequestParameter(data, clientSocket);

                Thread thread = new Thread(new ParameterizedThreadStart(ManageRequest));
                thread.Start(reqParam);
            }
            
        }
        public bool ReceiveInfo(TcpClient clientSocket, out string dataFromClient)
        {
            dataFromClient = null;
            IPEndPoint remoteIpEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;
            NetworkStream networkStream = clientSocket.GetStream();
            byte[] bytesFrom = new byte[10025];
            byte[] size = new byte[sizeof(int) + 1];

            try
            {
                networkStream.Read(size, 0, sizeof(int));
            }
            catch(IOException ex)
            {
                return false;
            }
            
            int rez = BitConverter.ToInt32(size, 0);

            networkStream.Read(bytesFrom, 0, rez);
            dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
            dataFromClient = dataFromClient.Substring(0, rez);

            Console.WriteLine(">> Received Data for {0}:\n{1}", remoteIpEndPoint.Address, dataFromClient);

            return true;
        }

        public void SendInfo(TcpClient clientSocket, string dataToSend) 
        {
            IPEndPoint remoteIpEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;
            NetworkStream networkStream = clientSocket.GetStream();
            byte[] size2 = BitConverter.GetBytes(dataToSend.Length);
            networkStream.Write(size2, 0, sizeof(int));
            byte[] sendBytes = Encoding.ASCII.GetBytes(dataToSend);
            networkStream.Write(sendBytes, 0, sendBytes.Length);

            networkStream.Flush();

            Console.WriteLine(">> Confirmation Sent for {0}", remoteIpEndPoint.Address);
        }

        public void ManageRequest(object requestParameter)
        {
            RequestParameter requestParam = (RequestParameter)requestParameter;
            IPEndPoint remoteIpEndPoint = requestParam.ClientSocket.Client.RemoteEndPoint as IPEndPoint;

            string result = requestHandler.RequestDecoder(requestParam.ReceivedData, requestParam.ClientSocket).ToString();

            Console.WriteLine(">> Result for {0}: {1}", remoteIpEndPoint.Address, result);

            SendInfo(requestParam.ClientSocket, result);
        }


        public void SendRequestEvent(object sender, RequestEventArgs e)
        {
            TcpClient clientSocket = e.ClientSocket;
            string encodedRequest = e.encodedRequest;
            SendInfo(clientSocket, encodedRequest);
        }

    }
}
