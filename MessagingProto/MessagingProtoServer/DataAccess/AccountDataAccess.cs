﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoServer.DBManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DataAccess
{
    class AccountDataAccess
    {
        public AccountModel GetAccount(string username)
        {
            AccountModel account = new AccountModel();
            AccountDBManagement db = new AccountDBManagement();
            account = db.GetAccountFromDB(username);
            Console.WriteLine("RETRIEVED ACCOUNT: {0}", account.Username);
            return account;
        }

        public bool CheckUsernameUnique(string username)
        {
            AccountDBManagement db = new AccountDBManagement();
            bool checkResult = db.CheckUsernameUnique(username);
            return checkResult;
        }

        public bool CheckEmailUnique(string email)
        {
            AccountDBManagement db = new AccountDBManagement();
            bool checkResult = db.CheckUsernameUnique(email);
            return checkResult;
        }

        public List<AccountModel> GetAllAccounts()
        {
            List<AccountModel> accountsList = new List<AccountModel>();
            AccountDBManagement db = new AccountDBManagement();
            accountsList = db.GetAllAccountsFromDB();
            return accountsList;
        }

        public bool CheckPasswordMatches(string username, string password)
        {
            AccountDBManagement db = new AccountDBManagement();
            bool checkResult = db.CheckPasswordMatches(username, password);
            return checkResult;
        }

        public void SetAccountPassword(string username, string password)
        {
            AccountDBManagement db = new AccountDBManagement();
            db.SetAccountPassword(username, password);
        }

        public void SetAccountIPAddress(string username, string ipaddress)
        {
            AccountDBManagement db = new AccountDBManagement();
            db.SetAccountIpAddress(username, ipaddress);
        }

        public void AddAccount(AccountModel account)
        {
            AccountDBManagement db = new AccountDBManagement();
            db.AddAccountToDB(account);
        }

        public void DeleteAccount(string username)
        {
            AccountDBManagement db = new AccountDBManagement();
            db.DeleteFromDB(username);
        }
    }
}
