﻿using CommonClasses.CommonModels;
using MessagingProtoServer.DBManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DataAccess
{
    class MessagesDataAccess
    {
        public List<MessagesModel> GetMessages(string sender, string receiver)
        {
            List<MessagesModel> messageList = new List<MessagesModel>();
            MessagesDBManagement db = new MessagesDBManagement();
            messageList = db.GetMessagesFromDB(sender, receiver);

            return messageList;
        }

        public void AddMessage(MessagesModel message)
        {
            MessagesDBManagement db = new MessagesDBManagement();
            db.AddToDB(message);
        }
    }
}
