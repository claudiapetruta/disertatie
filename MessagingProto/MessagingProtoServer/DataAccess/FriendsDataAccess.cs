﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoServer.DBManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.DataAccess
{
    class FriendsDataAccess
    {
        public List<FriendsModel> GetFriendsList(string username)
        {
            List<FriendsModel> friendsList = new List<FriendsModel>();
            FriendsDBManagement db = new FriendsDBManagement();
            friendsList = db.GetFriendListFromDB(username);
            
            return friendsList;
        }

        public List<FriendsModel> GetPendingRequests(string username)
        {
            List<FriendsModel> pendingList = new List<FriendsModel>();
            FriendsDBManagement db = new FriendsDBManagement();
            pendingList = db.GetPendingRequestsFromDB(username);
            
            return pendingList;
        }

        public List<FriendsModel> GetBlockedUsers(string username)
        {
            List<FriendsModel> blockedList = new List<FriendsModel>();
            FriendsDBManagement db = new FriendsDBManagement();
            blockedList = db.GetBlockedUsersFromDB(username);
            return blockedList;
        }

        public void AddFriend(string initiatorUsername, string targetUsername, RelationshipStatus relationship)
        {
            FriendsDBManagement db = new FriendsDBManagement();
            db.AddFriendToDB(initiatorUsername, targetUsername, relationship);
        }

        public void DeleteFriendship(int id)
        {
            FriendsDBManagement db = new FriendsDBManagement();
            db.DeleteFriendshipFromDB(id);
        }

        public bool UpdateFriendshipStatus(int id, RelationshipStatus relationship)
        {
            FriendsDBManagement db = new FriendsDBManagement();
            try
            {
                db.UpdateFriendshipStatus(id, relationship);
                return true;
            }catch (Exception ex)
            {
                return false;
            }
            
        }

        public bool CheckIfFriendshipExists(string username, string friend)
        {
            FriendsDBManagement db = new FriendsDBManagement();
            return db.CheckIfFriendshipExistsDB(username, friend);
        }
    }
}
