﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.UserConnectionRepository
{
    public class ConnectedUsersRepository
    {
        List<ConnectedUser> connectedUsersList = new List<ConnectedUser>();

        public ConnectedUser GetConnectedUserByUsername(string username)
        {
            ConnectedUser connectedUser = connectedUsersList.SingleOrDefault(s => s.Username == username);

            return connectedUser;
        }

        public ConnectedUser GetConnectedUserBySocket(TcpClient socket)
        {
            ConnectedUser connectedUser = connectedUsersList.Single(s => s.ClientSocket == socket);

            return connectedUser;
        }

        public void AddConnectedUser(ConnectedUser connectedUser)
        {
            connectedUsersList.Add(connectedUser);
        }

        public void RemoveUser(TcpClient clientSocket)
        {
            connectedUsersList.Remove(connectedUsersList.Single(s => s.ClientSocket == clientSocket));
        }

        public string CheckIfUserIsConnected(string username)
        {
            return connectedUsersList.Any(s => s.Username == username).ToString();
        }
    }

}
