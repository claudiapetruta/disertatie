﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.UserConnectionRepository
{
    public class ConnectedUser
    {
        public string Username { get; set; }
        public TcpClient ClientSocket { get; set; }

        public ConnectedUser(string username,  TcpClient socket)
        {
            Username = username;
            ClientSocket = socket;
        }
    }
}
