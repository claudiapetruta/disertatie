﻿using CommonClasses.CommonModels;
using MessagingProtoServer.BusinessLogic;
using MessagingProtoServer.CommunicationSystem;
using MessagingProtoServer.DBManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ServerConnectionHandler connection = new ServerConnectionHandler();
            connection.StartListening();
        }
    }
}
