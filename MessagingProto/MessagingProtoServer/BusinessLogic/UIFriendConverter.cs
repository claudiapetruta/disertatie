﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.BusinessLogic
{
    class UIFriendConverter
    {
        public static List<UIFriendModel> ConvertFriendListForUI(List<FriendsModel> friendList, string username)
        {
            AccountDataAccess dataAccess = new AccountDataAccess();
            List<UIFriendModel> uiFriendList = new List<UIFriendModel>();
            foreach(FriendsModel friend in friendList)
            {
                if (friend.InitiatorUser == username)
                {
                    
                    UIFriendModel uiFriend = new UIFriendModel(friend.Id, friend.TargetUser);
                    uiFriendList.Add(uiFriend);
                }
                else
                {
                    UIFriendModel uiFriend = new UIFriendModel(friend.Id, friend.InitiatorUser);
                    uiFriendList.Add(uiFriend);
                }
            }
            return uiFriendList.OrderByDescending(o => o.FriendOnlineStatus).ToList();
        }
    }
}
