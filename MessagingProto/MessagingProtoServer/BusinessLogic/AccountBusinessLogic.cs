﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoServer.DataAccess;
using MessagingProtoServer.DBManagement;

namespace MessagingProtoServer.BusinessLogic
{
    public class AccountBusinessLogic
    {
        public string AccountRegistration(AccountModel account)
        {
            AccountDataAccess dataAccess = new AccountDataAccess();

            if(dataAccess.CheckUsernameUnique(account.Username) && dataAccess.CheckEmailUnique(account.Email))
            {
                dataAccess.AddAccount(account);
                return "True";
            }
            return "False";
        }

        public string AccountLogin(string username, string password)
        {
            AccountDataAccess dataAccess = new AccountDataAccess();
            if(dataAccess.CheckPasswordMatches(username, password) )
            {
                return "True";
            }
            return "False";
        }
    }
}
