﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using CommonClasses.CommonUtils;
using MessagingProtoServer.CommunicationSystem;
using MessagingProtoServer.DataAccess;
using MessagingProtoServer.UserConnectionRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.BusinessLogic
{
    class HandlerMethods
    {
        FriendsModel relationship;
        List<UIFriendModel> friendList = new List<UIFriendModel>();
        List<FriendsModel> pendingList = new List<FriendsModel>();
        AccountBusinessLogic accountBusinessLogic = new AccountBusinessLogic();
        FriendsBusinessLogic friendsBusinessLogic = new FriendsBusinessLogic();
        AccountDataAccess dataAccess = new AccountDataAccess();
        RequestEncoder encoder = new RequestEncoder();
        public event EventHandler<RequestEventArgs> SendRequestEvent;

        public string UserRegistrationHandler(dynamic data)
        {
            AccountModel account = JsonConvert.DeserializeObject<AccountModel>(data.SerializedJson.ToString());

            string result = accountBusinessLogic.AccountRegistration(account);

            if (result == "True")
            {
                Console.WriteLine("Account created");
            }

            return result;
        }

        public string UserLoginHandler(dynamic data, ConnectedUsersRepository connectedUsersList, TcpClient socket)
        {
            Console.WriteLine(data.SerializedJson);
            LoginModel loginDetails = JsonConvert.DeserializeObject<LoginModel>(data.SerializedJson.ToString());
            string result = accountBusinessLogic.AccountLogin(loginDetails.Username, loginDetails.PasswordHash);
            if (result == "True")
            {
                Console.WriteLine("Logged in.");
                connectedUsersList.AddConnectedUser(new ConnectedUser(loginDetails.Username, socket));
            }
            else
            {
                Console.WriteLine("Couldn't find the login credentials used.");
            }

            return result;
        }

        public string AddFriendHandler(dynamic data, ConnectedUsersRepository connectedUsersList)
        {
            ConnectedUser connectedUser = null;
            Console.WriteLine(data.SerializedJson);

            relationship = JsonConvert.DeserializeObject<FriendsModel>(data.SerializedJson.ToString());

            string requestResult = friendsBusinessLogic.AddFriend(relationship.InitiatorUser, relationship.TargetUser);
            string result = encoder.EncodeRequest(RequestTypes.AddFriendReply, requestResult);
            string updatePending = encoder.EncodeRequest(RequestTypes.NotifyUpdatePendingList, "");
            Console.WriteLine("The result string is: {0}", result);

            if (requestResult == "True")
            {
                Console.WriteLine("Friend added.");
            }
            else
            {
                Console.WriteLine("Something went wrong");
            }

            connectedUser = connectedUsersList.GetConnectedUserByUsername(relationship.TargetUser);

            if (null == connectedUser)
            {
                //user not online
                return result;
            }
           

            SendRequestEventMethod(connectedUser.ClientSocket, updatePending);

            return result;
        }

        public string GetOnlineStatus(dynamic data, ConnectedUsersRepository connectedUsersList)
        {
            Console.WriteLine(data.SerializedJson);
            string receivedUsername = JsonConvert.DeserializeObject(data.SerializedJson.ToString());
            string result = connectedUsersList.CheckIfUserIsConnected(receivedUsername);
            if (result == "True")
            {
                Console.WriteLine(">>Status retrieved.");
            }
            else
            {
                Console.WriteLine(">>Something Went Wrong.");
            }

            return result;
        }

        public string GetFriendListHandler(dynamic data, ConnectedUsersRepository connectedUsersList)
        {
            string username = JsonConvert.DeserializeObject(data.SerializedJson.ToString());

            friendList = friendsBusinessLogic.GetFriendList(username);

            foreach (UIFriendModel friend in friendList)
            {
                if (connectedUsersList.CheckIfUserIsConnected(friend.FriendToShow) == "True")
                {
                    friend.FriendOnlineStatus = OnlineStatus.Online;
                }
                else
                {
                    friend.FriendOnlineStatus = OnlineStatus.Offline;
                }
            }

            string result = encoder.EncodeRequest(RequestTypes.GetFriendsListReply, friendList);

            if (friendList != null)
            {
                Console.WriteLine("Friend list obtained.");
            }
            else
            {
                Console.WriteLine("The user has no friends.");
            }

            return result;
        }

        public string GetPendingListHandler(dynamic data)
        {
            Console.WriteLine(data.SerializedJson);
            string usernamePending = JsonConvert.DeserializeObject(data.SerializedJson.ToString());
            pendingList = friendsBusinessLogic.GetPendingList(usernamePending);
            string result = encoder.EncodeRequest(RequestTypes.GetPendingListReply, pendingList);
            if (pendingList != null)
            {
                Console.WriteLine("Pending list obtained.");
            }
            else
            {
                Console.WriteLine("The user has no pending requests.");
            }

            return result;
        }

        public string ChangeFriendshipStatus(dynamic data)
        {
            Console.WriteLine(data.SerializedJson);
            FriendReplyModel friendReply = JsonConvert.DeserializeObject<FriendReplyModel>(data.SerializedJson.ToString());
            string requestResult = friendsBusinessLogic.ChangeFriendshipStatus(friendReply.Id, friendReply.Status);
            string result = encoder.EncodeRequest(RequestTypes.ChangeFriendshipStatusReply, requestResult);
            if (requestResult == "False")
            {
                Console.WriteLine("The status couldn't be changed.");
            }
            else
            {
                Console.WriteLine(">>The status of the friendship has been modified to: {0}", friendReply.Status);
            }

            

            return result;
        }

        public string DeleteFriendshipHandler(dynamic data)
        {
            Console.WriteLine(data.SerializedJson);
            FriendReplyModel friendReply = JsonConvert.DeserializeObject<FriendReplyModel>(data.SerializedJson.ToString());
            string requestResult = friendsBusinessLogic.DeleteFriendship(friendReply.Id);
            string result = encoder.EncodeRequest(RequestTypes.DeleteFriendshipReply, requestResult);
            if (requestResult == "False")
            {
                Console.WriteLine(">>The friendship couldn't be deleted.");
            }
            else
            {
                Console.WriteLine(">>The friendship has been deleted.");
            }

            return result;
        }

        public string ManageMessageSending(dynamic data, ConnectedUsersRepository connectedUsersList, TcpClient socket)
        {
            string result = "nothing";

            MessagesModel message = JsonConvert.DeserializeObject<MessagesModel>(data.SerializedJson.ToString());

            Console.WriteLine("receive message: {0} and should be send to {1}", message.MessageBody, message.Destination);

            ConnectedUser senderConnectedUser = connectedUsersList.GetConnectedUserBySocket(socket);
            
            if ( null == senderConnectedUser)
            {
                result = encoder.EncodeRequest(RequestTypes.NoAuthenticationIssue, "");
                return result;
            }

            message.Sender = senderConnectedUser.Username;
            message.MessageTimeStamp = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss");
            MessagesBusinessLogic messageBusLogic = new MessagesBusinessLogic();
            messageBusLogic.AddMessage(message);
            Console.WriteLine("message {0} from {1} added in bd and should be sent to {2} at time {3}", message.MessageBody, message.Sender, message.Destination, message.MessageTimeStamp);
            ConnectedUser destConnectedUser = connectedUsersList.GetConnectedUserByUsername(message.Destination);
            if ( null != destConnectedUser )
            {
                string destmessagerequest = encoder.EncodeRequest(RequestTypes.ReceiveMessage, message);
                SendRequestEventMethod(destConnectedUser.ClientSocket, destmessagerequest);
                Console.WriteLine("message {0} from {1} was sent to {2} at time {3}" , message.MessageBody, message.Sender ,message.Destination, message.MessageTimeStamp);
            }
            
            result = encoder.EncodeRequest(RequestTypes.SendMessageReply, message);
            

            return result;
        }


        protected virtual void SendRequestEventMethod(TcpClient clientSocket, string encodedRequest)
        {
            RequestEventArgs e = new RequestEventArgs();
            e.ClientSocket = clientSocket;
            e.encodedRequest = encodedRequest;
            EventHandler<RequestEventArgs> handler = SendRequestEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }



    }

    public class RequestEventArgs : EventArgs
    {
        public TcpClient ClientSocket { get; set; }
        public string encodedRequest { get; set; }

    }

}
