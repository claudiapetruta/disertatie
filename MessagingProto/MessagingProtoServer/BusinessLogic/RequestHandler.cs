﻿using CommonClasses.CommonEnums;
using CommonClasses.CommonModels;
using MessagingProtoServer.DataAccess;
using MessagingProtoServer.UserConnectionRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.BusinessLogic
{
    class RequestHandler
    {
        public ConnectedUsersRepository connectedUsersList = new ConnectedUsersRepository();
        public HandlerMethods handler = new HandlerMethods();
        public string RequestDecoder(string requestContent, TcpClient socket)
        {
            string result = null;
            
            Console.WriteLine("Request decoder data: {0}", requestContent);

            dynamic data = JsonConvert.DeserializeObject(requestContent);

            Console.WriteLine("The code is: {0}", data.RequestType);

            switch ((RequestTypes)data.RequestType)
            {
                case RequestTypes.Login:
                    result = handler.UserLoginHandler(data, connectedUsersList, socket);
                    break;

                case RequestTypes.Registration:
                    result = handler.UserRegistrationHandler(data);
                    break;
                case RequestTypes.AddFriend:
                    result = handler.AddFriendHandler(data, connectedUsersList);

                    break;
                case RequestTypes.GetOnlineStatus:
                    result = handler.GetOnlineStatus(data, connectedUsersList);

                    break;
                case RequestTypes.GetFriendsList:
                    result = handler.GetFriendListHandler(data, connectedUsersList);

                    break;
                case RequestTypes.GetPendingList:
                    result = handler.GetPendingListHandler(data);

                    break;
                case RequestTypes.ChangeFriendshipStatus:
                    result = handler.ChangeFriendshipStatus(data);

                    break;
                case RequestTypes.DeleteFriendship:
                    result = handler.DeleteFriendshipHandler(data);
                    break;

                case RequestTypes.SendMessage:
                    result = handler.ManageMessageSending(data, connectedUsersList, socket);
                    break;

                default:
                    Console.WriteLine("Something went wrong. The request couldn't be parsed");
                    break;
            }
            return result;
        }
    }
}
