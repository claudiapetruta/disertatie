﻿using MessagingProtoServer.DataAccess;
using System;
using System.Collections.Generic;
using CommonClasses.CommonModels;
using CommonClasses.CommonEnums;

namespace MessagingProtoServer.BusinessLogic
{
    class FriendsBusinessLogic
    {
        public string AddFriend(string initiatorUsername, string targetUsername)
        {
            FriendsDataAccess fdataAccess = new FriendsDataAccess();
            AccountDataAccess accDataAccess = new AccountDataAccess();

            try
            {
                if (fdataAccess.CheckIfFriendshipExists(initiatorUsername, targetUsername) == false 
                    && !string.IsNullOrEmpty(accDataAccess.GetAccount(targetUsername).Username))
                {
                    fdataAccess.AddFriend(initiatorUsername, targetUsername, RelationshipStatus.Pending);
                }
                else
                {
                    return "False";
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return "False";
            }
            return "True";
        }

        public List<UIFriendModel> GetFriendList(string username)
        {
            List<UIFriendModel> friendsList = new List<UIFriendModel>();
            FriendsDataAccess dataAccess = new FriendsDataAccess();
            friendsList = UIFriendConverter.ConvertFriendListForUI(dataAccess.GetFriendsList(username), username);
            
            return friendsList;
        }

        public List<FriendsModel> GetPendingList(string username)
        {
            List<FriendsModel> pendingList = new List<FriendsModel>();
            FriendsDataAccess dataAccess = new FriendsDataAccess();
            pendingList = dataAccess.GetPendingRequests(username);
            
            return pendingList;
        }

        public string ChangeFriendshipStatus(int id, RelationshipStatus status)
        {
            FriendsDataAccess dataAccess = new FriendsDataAccess();

            bool result = dataAccess.UpdateFriendshipStatus(id, status);

            if(result == true)
            {
                return status.ToString();
            }

            return result.ToString();
        }

        public string DeleteFriendship(int id)
        {
            FriendsDataAccess dataAccess = new FriendsDataAccess();
            try
            {
                dataAccess.DeleteFriendship(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "False";
            }
            return "True";
        }
    }
}
