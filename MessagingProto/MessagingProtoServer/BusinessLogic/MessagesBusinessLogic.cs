﻿using CommonClasses.CommonModels;
using MessagingProtoServer.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoServer.BusinessLogic
{
    class MessagesBusinessLogic
    {
        public string AddMessage(MessagesModel message)
        {
            MessagesDataAccess data = new MessagesDataAccess();
            try
            {
                data.AddMessage(message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "False";
            }
            return "True";
        }

        public List<MessagesModel> GetMessagesList(string sender, string receiver)
        {
            List<MessagesModel> messageList = new List<MessagesModel>();
            MessagesDataAccess data = new MessagesDataAccess();
            messageList = data.GetMessages(sender, receiver);

            return messageList;
        }
    }
}
