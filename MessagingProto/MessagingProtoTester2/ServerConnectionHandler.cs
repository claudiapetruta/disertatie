﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester2
{
    class ServerConnectionHandler
    {
        public static string StartListening()
        { 
            IPAddress ipAddress = Dns.GetHostEntry("localhost").AddressList[1];
            TcpListener serverSocket = new TcpListener(ipAddress, 5373);
            TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            Console.WriteLine(">> Server Started");
            clientSocket = serverSocket.AcceptTcpClient();
            string data = SendReply(serverSocket, clientSocket);
            return data;
        }

        public static string SendReply(TcpListener serverSocket, TcpClient clientSocket)
        {
            int requestCount = 0;
            Console.WriteLine(">> Connection from Client accepted.");
            requestCount = 0;
            requestCount = requestCount + 1;
            NetworkStream networkStream = clientSocket.GetStream();
            byte[] bytesFrom = new byte[100000];
            byte[] size = new byte[sizeof(int) + 1];
            networkStream.Read(size, 0, sizeof(int));
            int rez = BitConverter.ToInt32(size, 0);
            networkStream.Read(bytesFrom, 0, rez);
            string dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
            dataFromClient = dataFromClient.Substring(0, rez);
            Console.WriteLine(">> Received Data - " + dataFromClient);
            string serverResponse = ">> Message Received";
            byte[] size2 = BitConverter.GetBytes(serverResponse.Length);
            networkStream.Write(size2, 0, sizeof(int));
            byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);
            networkStream.Write(sendBytes, 0, sendBytes.Length);
            networkStream.Flush();
            Console.Write(">> Confirmation Sent");
            Console.ReadKey();
            return dataFromClient;

        }
    }
}
