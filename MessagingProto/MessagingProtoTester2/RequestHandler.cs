﻿using CommonClasses.CommonModels;
using MessagingProtoServer.BusinessLogic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester2
{
    class RequestHandler
    {
        public void RequestDecoder(string requestContent)
        {
            AccountBusinessLogic businessLogic = new AccountBusinessLogic();
            dynamic data = JsonConvert.DeserializeObject(requestContent);
            switch ((short)data.RequestType)
            {
                case 1:
                    Console.WriteLine(data.SerializedJson);
                    AccountModel account = JsonConvert.DeserializeObject<AccountModel>(data.SerializedJson.ToString());
                    Console.WriteLine("Account created");
                    Console.ReadKey();
                    break;
                case 2:
                    Console.WriteLine("Account logged in.");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Something went wrong. The request couldn't be parsed");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
