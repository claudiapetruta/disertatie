﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester2
{
    class Program
    {
        static void Main(string[] args)
        {
            string dataFromClient = ServerConnectionHandler.StartListening();
            RequestHandler request = new RequestHandler();
            request.RequestDecoder(dataFromClient);
        }

    }
}
