﻿using CommonClasses.CommonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester
{
    class Program
    {
        static void Main(string[] args)
        {
            RequestEncoder encoder = new RequestEncoder();
            AccountModel account = new AccountModel("username", "password", "email", "ipaddress");
            short request = (short)RequestTypes.Registration;
            string infoToSend = encoder.EncodeRequest(request, account);
            ClientConnectionHandler.StartConnection("127.0.0.1");
            ClientConnectionHandler.SendInformation(infoToSend);
        }
    }
}
