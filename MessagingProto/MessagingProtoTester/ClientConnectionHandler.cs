﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester
{
    class ClientConnectionHandler
    {
        static private TcpClient clientSocket = new TcpClient();
        static private int port = 5373;

        public static void StartConnection(string ipAddress)
        {
            Console.WriteLine(">> Client Started");
            clientSocket.Connect(ipAddress, port);
        }

        public static byte[] SendInformation(string infoToSend)
        {
            byte[] byteArray;
            byte[] size2 = new byte[sizeof(int) + 1];
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] text = System.Text.Encoding.ASCII.GetBytes(infoToSend);
            byteArray = BitConverter.GetBytes(text.Length);
            serverStream.Write(byteArray, 0, sizeof(int));
            serverStream.Flush();
            serverStream.Write(text, 0, text.Length);
            serverStream.Flush();
            serverStream.Read(size2, 0, sizeof(int));
            int rez = BitConverter.ToInt32(size2, 0);
            byte[] inStream = new byte[rez];
            serverStream.Read(inStream, 0, rez);
            string returndata = System.Text.Encoding.ASCII.GetString(inStream);
            Console.WriteLine(returndata);
            Console.ReadKey();
            return inStream;
        }
    }
}
