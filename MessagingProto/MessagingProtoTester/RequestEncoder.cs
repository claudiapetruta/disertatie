﻿using CommonClasses.CommonModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingProtoTester
{
    class RequestEncoder
    {
        public string EncodeRequest<T>(short requestType, T encodedClass)
        {
            string serializedJson = JsonConvert.SerializeObject(encodedClass);
            dynamic jsonObject = new JObject();
            jsonObject.RequestType = requestType;
            jsonObject.SerializedJson = serializedJson;
            Console.WriteLine(jsonObject.ToString());
            return jsonObject.ToString();
        }
    }
}
